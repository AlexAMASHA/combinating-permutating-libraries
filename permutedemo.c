//Headers included.
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <math.h>
#include "cpl.h"

/*
* A recursive permutation generator that operates by using recursion
* an array, w, with valid elements in the array, s. This produces all
* permutations of length n. The user can optionally write to a
* file specified via the FILE pointer P.
* 
*
* Alexander Sharif.
*/

//~ //------------------Function Prototypes--------------------------------/
//~ //Get user input (the size of the permutation)
//~ void input(char w [101],char s [101],char p[50],int* n);
//~ //Perform permutation generation.
//~ void recPerm(char word [101],char set [101],char path[50],int length,FILE* f,int iteration);
//~ 
//~ //------------------Function Definitions--------------------------------/
//------------------Function Prototypes--------------------------------/
//Perform general permutation generation from a set.



void input(char w [101],char s [101],char p[50],int* n)
{
    
    //Clear input buffer
    setbuf(stdin,0);
    
    //Prompt user for a non-zero positive integer n.
    printf("Set n|n<=0 to quit...n");
    
    //Reinitalize n
    *n=0;
    
    //Prompt user for optional path, dropping newline.
    printf("\nP: ");
    fgets(p,49,stdin);
    p[strlen(p)-1]=0;
    
    //Prompt user for set, dropping newline.z
    printf("S: ");
    fgets(s,100,stdin);
    s[strlen(s)-1]=0;
    
    //Prompt user for n.
    printf("N: ");
    scanf("%d",n);
    
}

int main()
{
    
    //clock_t structure t of point of execution of permutation generation.
    clock_t t;
    
    //Let w be the array of the current permutation, s be the array of
    //permitted symbols, and p be the optional path.
    char w [101],s [101],p[50];
    
    //Pointer to file &p
    FILE *f;
    
    //Let n be the size of all permutations, and h the index of the
    //permutation array w.
    int n=1;
    
    //While n is a positive natural number, execute.
    while (n>0)
    {
        
        //Get input
        input(w,s,p,&n);
        
        //Initialize w with initial symbol in p.
        memset(w,s[0],n);
        
        //Nullify the remaining upper bound.
        memset(w+n,0,50);
        
        //~ //Fail gracefully.
        //~ if(n<=0)
        //~ {
            //~ 
            //~ exit(1);
            //~ 
        //~ }
        
        
        //Open file p with file pointer f.
        if(strcmp(p,"")!=0)
			f=fopen(p,"w");
        
        //Get time approximation at initial computations for recPerms.
        t=clock();
        recPerm(w,s,p,n,f,0);
        
        //Get duration of execution.
        t=clock()-t;
        
        //Close the file defined at p if applicable.
        if(strcmp(p,"")!=0)
        fclose(f);
        //Print time
        printf("\nCPU seconds elapsed since initializing computations: %f\n",(double)t/CLOCKS_PER_SEC);
        //system("pause");
        //system("cls");
        
    }
    
    return 0;
    
}