//Headers included.
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <limits.h>

/*
* Recursive/iterative permutation generation libraries.
* Alexander Sharif.
*/

//------------------Function Prototypes--------------------------------/
//Perform general permutation generation from a set.
void recPerm(char word [101],char set [101],char path[50],int length,FILE* f,int iteration);
void itPerm(char word [101],char set [101],char path[50],int length,FILE* file);
void recPermString(char word [101],char set [101],char path[50],int length,FILE* f,int iteration);
void itPermString(char word [101],char set [101],char path[50],int length,FILE* file);


/*
 * 
 * name: itPerm
 * @param word-The string used for holding the permutation in memory.
 * 
 * @param set-The string used for holding the set of valid characters
 * used in the generation of permutations.
 * 
 * @param path-The string used for holding the path of the file to write to
 * if necessary.
 * 
 * @param length-The integer used to ensure that the recursive call halts.
 * 
 * @param file-The file used for writing the permutations to a file.
 * 
 * @return
 * 
 */
 
void itPerm(char word [101],char set [101],char path[50],int length,FILE* file)
{
	//The permutation round.
	for(round=0; round<pow(strlen(set),n); round++)
    {
		//Write the appropriate element to the permutation to be created.
		for(index=0;index<=n;index++)

			word[n-index]=set[int(round/pow(strlen(set),index))%strlen(set)];

		//Reinitialize string
		memset(word+n,0,100);

		//Write permutation.
		printf("%s\n",word);

		//Write if applicable.
		if(strcmp(p,"")!=0)

			fprintf(file,"%s\n",word);
    }

	//Close file if applicable.
    if(strcmpi(p,"")!=0)

		fclose(file);
}

/*
 * 
 * name: recPerm
 * @param word-The string used for holding the permutation in memory.
 * 
 * @param set-The string used for holding the set of valid characters
 * used in the generation of permutations.
 * 
 * @param path-The string used for holding the path of the file to write to
 * if necessary.
 * 
 * @param length-The integer used to ensure that the recursive call halts.
 * 
 * @param file-The file used for writing the permutations to a file.
 * 
 * @param iteration-The integer used for holding counting the calls and used to
 * ensure a terminating calling case.
 * 
 * @return
 * 
 */
void recPerm(char word [101],char set [101],char path[50],int length,FILE* file,int iteration)
{
    
    //Let index be the index of array set, the set of permitted symbols.
    int index;
    
    //Initialize the upper bounds of word to 0.
    memset(word+length,0,50);
    
    //While iteration, the index of the permutation array iteration, is less than the size
    //of the permutations.
    if(iteration<length)
    {
        for (index=0;index<strlen(set);index++)
        {
            
            //Set the index to the next character in the set of symbols.
            word[iteration]=set[index];
            //Move the index of word,iteration, up by one.
            recPerm(word,set,path,length,file,iteration+1);
            
        }

    }
    
    else
    {
        
        //Write out permutation
        printf("%s\n",word);
        
        //If path was specified, write to file.
        if(strcmp(path,"")!=0)
        fprintf(file,"%s\n",word);
        
    }
    
}
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvDEMARKATION vvvvvvvvvvvvvvvvvvvvvvvvvvv

void itPerm(char word [101],char set [101],char path[50],int length,FILE* file)
{
	//The permutation round.
	for(round=0; round<pow(strlen(set),n); round++)
    {
		//Write the appropriate element to the permutation to be created.
		for(index=0;index<=n;index++)

			word[n-index]=set[int(round/pow(strlen(set),index))%strlen(set)];

		//Reinitialize string
		memset(word+n,0,100);

		//Write permutation.
		printf("%s\n",word);

		//Write if applicable.
		if(strcmp(p,"")!=0)

			fprintf(file,"%s\n",word);
    }

	//Close file if applicable.
    if(strcmpi(p,"")!=0)

		fclose(file);
}

/*
 * 
 * name: recPerm
 * @param word-The string used for holding the permutation in memory.
 * 
 * @param set-The string used for holding the set of valid characters
 * used in the generation of permutations.
 * 
 * @param path-The string used for holding the path of the file to write to
 * if necessary.
 * 
 * @param length-The integer used to ensure that the recursive call halts.
 * 
 * @param file-The file used for writing the permutations to a file.
 * 
 * @param iteration-The integer used for holding counting the calls and used to
 * ensure a terminating calling case.
 * 
 * @return
 * 
 */
void recPerm(char word [101],char set [101],char path[50],int length,FILE* file,int iteration)
{
    
    //Let index be the index of array set, the set of permitted symbols.
    int index;
    
    //Initialize the upper bounds of word to 0.
    memset(word+length,0,50);
    
    //While iteration, the index of the permutation array iteration, is less than the size
    //of the permutations.
    if(iteration<length)
    {
        for (index=0;index<strlen(set);index++)
        {
            
            //Set the index to the next character in the set of symbols.
            word[iteration]=set[index];
            //Move the index of word,iteration, up by one.
            recPerm(word,set,path,length,file,iteration+1);
            
        }

    }
    
    else
    {
        
        //Write out permutation
        printf("%s\n",word);
        
        //If path was specified, write to file.
        if(strcmp(path,"")!=0)
        fprintf(file,"%s\n",word);
        
    }
    
}

